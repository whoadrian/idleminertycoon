﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Singleton for calling UI panels and menus

public class UIManager : MonoBehaviour
{
	public static UIManager instance;

	public ManagersMenu managersMenu;
	public SectorMenu sectorMenu;

	private void Awake()
	{
		instance = this;
	} 
}

