﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// Class used for displaying the amount of resources in a ResourcePoint, per resource type

public class ResourceDisplay : MonoBehaviour
{
	public Text text;
	public Resource.Type resourceType;
	public ResourcePoint resourcePoint;

	// Helper, cache values
	protected float _currentResourceAmount = -1;

	private void Update()
	{
		if (resourcePoint == null)
			return;

		float totalResources = resourcePoint.GetAmount(resourceType);

		if (totalResources >= 0 && _currentResourceAmount != totalResources)
		{
			_currentResourceAmount = totalResources;
			if (text)
				text.text = totalResources.ToString("0.##");
		}
	} 
}

