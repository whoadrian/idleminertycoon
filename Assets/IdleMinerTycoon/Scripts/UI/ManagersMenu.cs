﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// Menu for displaying all managers related to a mine sector. (elevator, mineshaft, warehouse managers)

public class ManagersMenu : MonoBehaviour
{
	public ManagerDisplayInfo assignedManagerInfo; // The panel for this sector's assigned manager
	public ManagerDisplayInfo unassignedManagerInfo; // Template panel for unassigned managers
	[Space]
	public Text costText; // Cost to buy a new manager (text)

	// Helper list
	protected List<ManagerDisplayInfo> extraManagerUnassignedInfo = new List<ManagerDisplayInfo>();

	// The sector we're displaying info about
	protected MineSector _sector;
	// All unassigned managers for this type of sector (helper)
	protected List<SectorManager> _unassignedManagers = new List<SectorManager>();

	// Cache total cost and update only on changes
	private float _cachedTotalCoins = -1;

	public void Update()
	{
		if (costText)
		{
			// Update cost for managers
			float totalCoins = LevelManager.Instance.warehouse.dropoffPoint.resource.GetAmount(Resource.Type.Coin);
			if (totalCoins != _cachedTotalCoins)
			{
				_cachedTotalCoins = totalCoins;

				float cost = GameManager.Instance.config.additionalCosts.initialManagerCost;

				// Deduce how much the next manager should cost based on the number of already bought managers
				Utils.ApplyPercentageIncreaseOverTime(ref cost, GameManager.Instance.config.additionalCosts.managerCostPercentageIncreaseWithCount / 100.0f, _unassignedManagers.Count);

				costText.text = cost.ToString();

				// Red for NOPE, green for CAN BUY
				if (totalCoins < cost)
				{
					costText.color = Color.red;
				}
				else
				{
					costText.color = Color.green;
				}
			}
		}
	}

	// Show menu of managers, setup everything in the UI
	public void Show(MineSector sector, List<SectorManager> unassignedManagers)
	{
		_sector = sector;
		_unassignedManagers = unassignedManagers;
		_cachedTotalCoins = -1;

		// Setup currently assigned manager (if any)
		if (sector.CurrentManager)
		{
			assignedManagerInfo.Setup(sector.CurrentManager);
			assignedManagerInfo.gameObject.SetActive(true);

			assignedManagerInfo.OnManagerUnassign += OnUnassignManager;
			assignedManagerInfo.OnManagerSell += OnUnassignManager;
			assignedManagerInfo.OnManagerSell += SellManager;
		}
		else
		{
			assignedManagerInfo.gameObject.SetActive(false);
		}

		// Setup currently unassigned managers (if any)
		if (unassignedManagers.Count == 0)
		{
			unassignedManagerInfo.gameObject.SetActive(false);
		}
		else
		{
			unassignedManagerInfo.gameObject.SetActive(true);

			for (int i=0; i<unassignedManagers.Count; i++)
			{
				// Use template if first unassigned manager
				if (i == 0)
				{
					unassignedManagerInfo.Setup(unassignedManagers[i]);
					unassignedManagerInfo.OnManagerAssign += OnAssignManager;
					unassignedManagerInfo.OnManagerSell += SellManager;
				}
				// Instantiate template if more than one unassigned managers
				else
				{
					ManagerDisplayInfo newInfo = Instantiate(unassignedManagerInfo, unassignedManagerInfo.transform.parent);
					newInfo.Setup(unassignedManagers[i]);
					newInfo.OnManagerAssign += OnAssignManager;
					newInfo.OnManagerSell += SellManager;
					extraManagerUnassignedInfo.Add(newInfo);
				}
			}
		}

		// Show
		gameObject.SetActive(true);
	}

	// Close menu
	public void Hide()
	{
		gameObject.SetActive(false);

		for (int i = extraManagerUnassignedInfo.Count - 1; i >= 0; i--)
			Destroy(extraManagerUnassignedInfo[i].gameObject);
		extraManagerUnassignedInfo.Clear();
	}



	// UI button callback. Assign a manager to the sector we're representing
	public void OnAssignManager (SectorManager manager)
	{
		_sector.AssignManager(manager);
		Refresh();
	}

	// UI button callback. Unassign manager from the sector we're representing
	public void OnUnassignManager (SectorManager manager)
	{
		_sector.UnassignCurrentManager();
		Refresh();
	}

	// UI button callback. Sell a manager
	protected void SellManager(SectorManager manager)
	{
		// TO DO: resource management should be done in a higher level script
		LevelManager.Instance.warehouse.dropoffPoint.resource.AddAmount(manager.managerCost, Resource.Type.Coin);
		LevelManager.Instance.ManagerSold(manager);
		Refresh();
	}

	// Buy a manager
	public void BuyManager ()
	{
		// Figure out costs based on the number of already bought managers, check if we can afford a new one.
		float totalCoins = LevelManager.Instance.warehouse.dropoffPoint.resource.GetAmount(Resource.Type.Coin);
		float cost = GameManager.Instance.config.additionalCosts.initialManagerCost;
		Utils.ApplyPercentageIncreaseOverTime(ref cost, GameManager.Instance.config.additionalCosts.managerCostPercentageIncreaseWithCount / 100.0f, _unassignedManagers.Count);

		if (totalCoins < cost)
			return;

		// TO DO: resource management should be done in a higher level script
		LevelManager.Instance.warehouse.dropoffPoint.resource.SubstractAmount(cost, Resource.Type.Coin);
		LevelManager.Instance.BuyManager(_sector, cost);
		Refresh();
	}

	// Refresh managers list
	void Refresh ()
	{
		Hide();
		Show(_sector, _unassignedManagers);
	}
}

