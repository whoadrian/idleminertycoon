﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// Display screen for a specific manager. Shows all its qualities, plus buttons for managing it.

public class ManagerDisplayInfo : MonoBehaviour
{
	[HideInInspector]
	public SectorManager manager; // Actual mine manager

	[Space]
	public Image managerSprite; 

	[Space]
	public Text nameField; 
	public Text typeField;
	public Text powerupField;
	public Text sellForField;

	[Space]
	public Button assignButton;
	public Button unassignButton;
	public Button sellButton;


	// Button callbacks for higher level scripts
	public System.Action<SectorManager> OnManagerAssign;
	public System.Action<SectorManager> OnManagerUnassign;
	public System.Action<SectorManager> OnManagerSell;


	// Fill all fields and initialise
	public void Setup (SectorManager manager)
	{
		this.manager = manager;

		managerSprite.sprite = manager.avatarData.menuSprite;
		nameField.text = manager.info.name;
		typeField.text = manager.type.ToString();
		powerupField.text = string.Format("{0} x{1} \nDuration: {2} | Reload Time: {3}", 
			manager.info.powerup.ToString(), manager.info.powerupAmount, manager.info.powerupTime, manager.info.powerupReloadTime);
		sellForField.text = manager.managerCost.ToString();
	}

	// No longer used, remove all listeners, etc.
	private void OnDisable()
	{
		OnManagerAssign = null;
		OnManagerUnassign = null;
		OnManagerSell = null;
	}


	// UI button callbacks

	public void OnSellClick()
	{
		if (OnManagerSell != null)
			OnManagerSell(manager);
	}

	public void OnAssignClick ()
	{
		if (OnManagerAssign != null)
			OnManagerAssign(manager);
	}

	public void OnUnassignClick ()
	{
		if (OnManagerUnassign != null)
			OnManagerUnassign(manager);
	}
}

