﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// Class used for displaying stats and upgrade options for a mine sector (warehouse, mineshaft, elevator)

public class SectorMenu : MonoBehaviour
{
	[Space]
	// UI texts
	public Text sectorIndex;
	public Text levelCount;
	public Text minersCount;
	public Text walkingSpeed;
	public Text miningSpeed;
	public Text workerCapacity;
	public Text levelupCost;

	// Sector we're representing
	protected MineSector _sector;


	// Show menu
	public void Show (MineSector sector)
	{
		_sector = sector;
		Refresh();

		gameObject.SetActive(true);
	}

	// Hide menu
	public void Hide ()
	{
		gameObject.SetActive(false);
	}

	// UI button callback. Increase this sector's level
	public void IncreaseLevel ()
	{
		// Check if we can aford it
		float totalMoney = LevelManager.Instance.warehouse.dropoffPoint.resource.GetAmount(Resource.Type.Coin);
		if (totalMoney < _sector.Params.levelupCost)
			return;

		// TO DO: Resource management should be done in a higher level script
		LevelManager.Instance.warehouse.dropoffPoint.resource.SubstractAmount(_sector.Params.levelupCost, Resource.Type.Coin);

		_sector.Level++;
		Refresh();
	}

	private void Update()
	{
		if (_sector == null)
			return;

		// Show if we can afford it. Red for NOPE, green for YUP
		if (levelupCost)
		{
			float totalMoney = LevelManager.Instance.warehouse.dropoffPoint.resource.GetAmount(Resource.Type.Coin);
			if (totalMoney >= _sector.Params.levelupCost)
			{
				levelupCost.color = Color.green;
			}
			else
			{
				levelupCost.color = Color.red;
			}
		}
	}

	// Refresh values if anything's changed
	protected void Refresh ()
	{
		if (_sector == null)
			return;

		if (_sector.GetType() == typeof(MineShaft))
			sectorIndex.text = ((MineShaft)_sector).Depth.ToString();
		else
			sectorIndex.text = string.Empty;

		levelCount.text = _sector.Level.ToString();
		minersCount.text = _sector.miners.Count.ToString();
		walkingSpeed.text = _sector.Params.walkingSpeed.ToString();
		miningSpeed.text = _sector.Params.miningSpeed.ToString();
		workerCapacity.text = _sector.Params.workerCapacity.ToString();
		levelupCost.text = _sector.Params.levelupCost.ToString();
	}
}

