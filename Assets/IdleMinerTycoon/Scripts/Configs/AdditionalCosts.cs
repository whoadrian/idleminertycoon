﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



// Various additional costs of the game

[CreateAssetMenu(fileName = "AdditionalCosts", menuName = "IdleMinerTycoon/AdditionalCosts", order = 1)]
public class AdditionalCosts : ScriptableObject
{
	[Header("New Mineshafts")]
	public float initialMineshaftCost = 100; // First mineshaft cost
	public int mineshaftCostPercentageIncreaseWithDepth = 10; // How much the cost increases as you build more mineshafts (long name, sorry!)

	[Header("Managers")]
	public float initialManagerCost = 50; // First manager cost
	public int managerCostPercentageIncreaseWithCount = 10; // How much the cost increases as the list of managers gets larger (long name, sorry!)
}
