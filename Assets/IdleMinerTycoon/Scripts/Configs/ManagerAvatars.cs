﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Holds references to all avatars the managers can have. 
// When a manager is instantiated, depending on its type, it chooses a random avatar from here.

[CreateAssetMenu(fileName = "ManagerAvatars", menuName = "IdleMinerTycoon/ManagerAvatars", order = 1)]
public class ManagerAvatars : ScriptableObject
{
	[System.Serializable]
	public class ManagerData
	{
		public Sprite menuSprite; // Sprite of manager as it shows up in the HUD menu
		public GameObject gameAvatar; // Prefab of the manager as it shows up in-game
	}

	[System.Serializable]
	public class ManagerType
	{
		public SectorManager.Type type; // Type of manager
		public List<ManagerData> data; // Collection of data (see above)
	}
	
	public List<ManagerType> data; // Actual data


	// Helper function, selects a random avatar for a specific type of manager
	public ManagerData GetRandomDataForType(SectorManager.Type type)
	{
		for (int i = 0; i < data.Count; i++)
			if (data[i].type == type)
				return data[i].data[Random.Range(0, data[i].data.Count)];
		return null;
	}
}

