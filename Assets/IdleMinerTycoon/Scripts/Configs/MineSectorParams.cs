﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif


// Shared data for all mine sectors (warehouse, elevator, mineshafts)

[CreateAssetMenu(fileName = "MineSectorParams", menuName = "IdleMinerTycoon/MineSectorParams", order = 1)]
public class MineSectorParams : ScriptableObject
{
	// Levels at which the boost will trigger
	public static int[] boostLevels = new int[] { 10, 20, 30, 40, 50, 100, 150, 200, 300, 400, 500, 750, 1000, 1300, 1600, 2000, 3000, 4000, 5000, 7000, 10000, 15000, 20000, 30000, 999999, int.MaxValue };

	// Type of config
	public enum Type
	{
		InitialSettings, // This config will apply when the mine sector gets instantiated
		Bonus // This config will apply as a bonus over the already existing parameters
	}
	public Type paramsType = Type.InitialSettings;

	[Header("Params")]
	public int miners; // Number of miners
	//public float totalExtraction;
	public float walkingSpeed; // Walking speed of miners
	public float miningSpeed; // Speed of collection/dropoff
	public float workerCapacity; // Amount of resources a miner can carry
	public float levelupCost; // Cost of leveling up

	// Deep copy helper function
	public void CopyFrom(MineSectorParams other)
	{
		this.miners = other.miners;
		//this.totalExtraction = other.totalExtraction;
		this.walkingSpeed = other.walkingSpeed;
		this.miningSpeed = other.miningSpeed;
		this.workerCapacity = other.workerCapacity;
		this.levelupCost = other.levelupCost;
	}

	// Apply bonus a number of times (growth rate over time)
	public static void ApplyBonus(ref MineSectorParams target, MineSectorParams bonus, int times = 1)
	{
		UnityEngine.Assertions.Assert.IsTrue(bonus.paramsType == Type.Bonus, "You're trying to apply a bonus with a non-bonus config!");

		target.miners += bonus.miners * times;
		//target.totalExtraction *= Mathf.Pow(1 + bonus.totalExtraction, times);
		Utils.ApplyPercentageIncreaseOverTime(ref target.walkingSpeed, bonus.walkingSpeed, times);
		Utils.ApplyPercentageIncreaseOverTime(ref target.miningSpeed, bonus.miningSpeed, times);
		Utils.ApplyPercentageIncreaseOverTime(ref target.workerCapacity, bonus.workerCapacity, times);
		Utils.ApplyPercentageIncreaseOverTime(ref target.levelupCost, bonus.levelupCost, times);
	}

	// Deduce current params from level, initial params and bonuses
	public static MineSectorParams CalculateCurrentParams(int level, MineSectorParams initialParams, MineSectorParams perLevelBonus, MineSectorParams perBoostBonus)
	{
		MineSectorParams result = ScriptableObject.CreateInstance<MineSectorParams>();
		result.CopyFrom(initialParams);
		
		for (int i=0; i<boostLevels.Length; i++)
		{
			int lastBoostLevel = (i == 0) ? 0 : boostLevels[i - 1];
			int nextBoostLevel = boostLevels[i];

			int levelBonusCount = Mathf.Min(level, nextBoostLevel - 1) - lastBoostLevel;
			ApplyBonus(ref result, perLevelBonus, levelBonusCount);

			if (level >= nextBoostLevel)
				ApplyBonus(ref result, perBoostBonus);

			if (level <= nextBoostLevel)
				break;
		}

		return result;
	}
}

#if UNITY_EDITOR

[CustomEditor(typeof(MineSectorParams))]
public class MineSectorParamsEditor : Editor
{
	public override void OnInspectorGUI()
	{
		MineSectorParams m = (MineSectorParams)target;

		EditorGUI.BeginChangeCheck();

		m.paramsType = (MineSectorParams.Type)EditorGUILayout.EnumPopup("Params Type", m.paramsType);

		EditorGUILayout.Separator();

		switch(m.paramsType)
		{
			// Config applies at instantiation. Values are copied.
			case MineSectorParams.Type.InitialSettings:
				m.miners = EditorGUILayout.IntField("Miners", m.miners);
				//m.totalExtraction = EditorGUILayout.FloatField("Total Extraction", m.totalExtraction);
				m.walkingSpeed = EditorGUILayout.FloatField("Walking Speed", m.walkingSpeed);
				m.miningSpeed = EditorGUILayout.FloatField("Mining Speed", m.miningSpeed);
				m.workerCapacity = EditorGUILayout.IntField("Worker Capacity", (int)m.workerCapacity);
				m.levelupCost = EditorGUILayout.FloatField("Levelup Cost", m.levelupCost);
				break;

			// Config applies as a bonus. Percentages and additions to existing params.
			case MineSectorParams.Type.Bonus:
				m.miners = EditorGUILayout.IntField("Miners (+)", m.miners);
				//m.totalExtraction = EditorGUILayout.IntSlider("Total Extraction (%)", (int)(m.totalExtraction * 100), 0, 100) / 100.0f;
				m.walkingSpeed = EditorGUILayout.Slider("Walking Speed (%)", (int)(m.walkingSpeed * 100), 0, 100) / 100.0f;
				m.miningSpeed = EditorGUILayout.Slider("Mining Speed (%)", (int)(m.miningSpeed * 100), 0, 100) / 100.0f;
				m.workerCapacity = EditorGUILayout.Slider("Worker Capacity (%)", (int)(m.workerCapacity * 100), 0, 100) / 100.0f;
				m.levelupCost = EditorGUILayout.Slider("Levelup Cost (%)", (int)(m.levelupCost * 100), 0, 100) / 100.0f;
				break;
		}
		
		if (EditorGUI.EndChangeCheck())
		{
			EditorUtility.SetDirty(m);
		}
	}
}

#endif

