﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// From here you can access all game's settings. Config from which all references start

[CreateAssetMenu(fileName = "GameConfiguration", menuName = "IdleMinerTycoon/GameConfiguration", order = 1)]
public class GameConfig : ScriptableObject
{
	[Header("Mine Shaft")]
	public MineSectorObjects mineShaftObjects; // Prefabs of the mine shaft
	[Space(2)]
	public MineSectorParams mineShaftInitialParams; // Params assigned when a mineshaft is instantiated 
	public MineSectorParams mineShaftPerLevelBonus; // Bonus topping the initial params (per level increase)
	public MineSectorParams mineShaftPerBoostBonus; // Bonus topping the initial params (per boost)
	public MineSectorParams mineShaftPerDepthBonus; // Bonus topping the initial params (per mine depth)

	[Header("Elevator")]
	public MineSectorObjects elevatorObjects; // Prefabs of the elevator
	[Space(2)]
	public MineSectorParams elevatorInitialParams; // Params assigned when the elevator is instantiated
	public MineSectorParams elevatorPerLevelBonus; // Bonus topping the initial params (per level increase)
	public MineSectorParams elevatorPerBoostBonus;

	[Header("Warehouse")]
	public MineSectorObjects warehouseObjects; // Prefabs of the warehouse
	[Space(2)]
	public MineSectorParams warehouseInitialParams; // Params assigned when the warehouse is instantiated 
	public MineSectorParams warehousePerLevelBonus; // Bonus topping the initial params (per level increase)
	public MineSectorParams warehousePerBoostBonus; // Bonus topping the initial params (per boost)

	[Header("Additional Costs")]
	public AdditionalCosts additionalCosts; // Additional costs of the game

	[Header("Starting Resources (Debug)")]
	public Resource startingResource;
}

