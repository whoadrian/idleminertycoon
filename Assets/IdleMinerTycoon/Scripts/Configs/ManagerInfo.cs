﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Pool of info about what a manager can do and its parameters. From here, a manager randomly selects its functionality, per type.

[CreateAssetMenu(fileName = "ManagerInfo", menuName = "IdleMinerTycoon/ManagerInfo", order = 1)]
public class ManagerInfo : ScriptableObject
{
	[System.Serializable]
	public class ManagerData // General data
	{
		public string name;
		public SectorManager.Powerup powerup;
		public float powerupAmount;
		public float powerupTime = 120; // seconds
		public float powerupReloadTime = 100; // seconds
	}

	[System.Serializable]
	public class ManagerType // Per-type data (see above)
	{
		public SectorManager.Type type;
		public List<ManagerData> data;
	}

	public List<ManagerType> data;


	// Helper function, retrieves a random data class for a specific type of manager
	public ManagerData GetRandomDataForType(SectorManager.Type type)
	{
		for (int i=0; i<data.Count; i++)
			if (data[i].type == type)
				return data[i].data[Random.Range(0, data[i].data.Count)];
		return null;
	}
}
