﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Prefab references for a mine sector

[CreateAssetMenu(fileName = "MineSectorObjects", menuName = "IdleMinerTycoon/MineSectorObjects", order = 1)]
public class MineSectorObjects : ScriptableObject
{
	public GameObject minerPrefab; // Miner this mine sector has
	public ManagerCollection managers; // Data about all managers this mine sector can have
}

