﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Holds references to all data related to managers - effects, info, visual avatars, etc.

[CreateAssetMenu(fileName = "ManagerCollection", menuName = "IdleMinerTycoon/ManagerCollection", order = 1)]
public class ManagerCollection : ScriptableObject
{
	[Space]
	public GameObject playerManager; // Manager controlled by the player
	public GameObject sectorManagerTemplate; // AI manager
	[Space]
	public ManagerInfo managersInfo; // Data pool for a manager's type
	public ManagerAvatars managersAvatars; // Data pool for a manager's visual avatar
}

