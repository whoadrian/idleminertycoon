﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Resources of the game
[System.Serializable]
public class Resource
{
	public enum Type { Coin }

	[Tooltip("Type of resource")]
	public Type type;

	[Tooltip("-1 for infinite resources")]
	public float amount = -1;
	
	public Resource (float amount, Type type)
	{
		this.amount = amount;
		this.type = type;
	}
}


// This class has an overview over all game elements and settings.

public class GameManager : MonoBehaviour
{
	// Singleton
	private static GameManager _instance;
	public static GameManager Instance
	{
		get
		{
			return _instance;
		}
	}

	// All game settings
	public GameConfig config;



	private void Awake()
	{
		UnityEngine.Assertions.Assert.raiseExceptions = true;

		// Make sure there's only one GameManager in the game at all times
		if (_instance == null)
		{
			_instance = this;

			// Keep this GameManager across all scenes
			DontDestroyOnLoad(gameObject);
		}
		else if (_instance != this)
		{
			Destroy(gameObject);
		}
	}
}

