﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Collection of resources. Used as a collection / dropoff point for workers

public class ResourcePoint : MonoBehaviour
{
	// All resources
	public List<Resource> resources;


	#region Helpers

	public float GetAmount(Resource.Type type)
	{
		foreach (Resource r in resources)
			if (r.type == type)
				return r.amount;
		return 0;
	}

	public void SubstractAmount(float amount, Resource.Type type)
	{
		for (int i = 0; i < resources.Count; i++)
			if (resources[i].type == type && resources[i].amount > 0)
				resources[i].amount = Mathf.Max(0, resources[i].amount - amount);
	}

	public void AddAmount(float amount, Resource.Type type)
	{
		bool set = false;
		for (int i = 0; i < resources.Count; i++)
		{
			Resource r = resources[i];
			if (r.type == type)
			{
				r.amount += amount;
				set = true;
			}
		}
		if (!set)
			resources.Add(new Resource(amount, type));
	}

	// Reverts all to 0
	public void Init()
	{
		foreach (Resource r in resources)
			r.amount = 0;
	}

	#endregion
}

