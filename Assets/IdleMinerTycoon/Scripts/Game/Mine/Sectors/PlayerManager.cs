﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// Manager through which the player can interact with the mine sector. Used when there's no AI manager
// TO DO: This name is a bit confusing.

public class PlayerManager : SectorManager
{
	// Button for interacting with the sector
	public Button button;

	// Called upon instantiation, by the sector
	public override void Setup(MineSector sector)
	{
		base.Setup(sector);

		// Listener setup
		if (button)
		{
			button.onClick.RemoveAllListeners();
			button.onClick.AddListener(OnButtonClick);
		}
	}

	// Miner callback, miner is ready and idle
	public override void OnMinersReady(Miner miner)
	{
		if (button && !button.enabled)
			button.enabled = true;
	}

	public override void OnMinerSpawned(Miner miner)
	{
		if (button && !button.enabled)
			button.enabled = true;
	}

	// UI button callback. Call to action for miners.
	public void OnButtonClick ()
	{
		for (int i = 0; i < _sector.miners.Count; i++)
			UrgeMiner(_sector.miners[i]);

		if (button)
			button.enabled = false;
	}
}

