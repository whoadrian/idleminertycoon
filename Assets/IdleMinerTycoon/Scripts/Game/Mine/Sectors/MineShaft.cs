﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif


// Mineshaft sector

public class MineShaft : MineSector
{
	public Text depthCount;

	// The depth at which this mineShaft has been spawned (shaft count)
	public int Depth
	{
		get { return transform.GetSiblingIndex(); }
	}
	
	// Prefab references for the mineshaft
	protected override MineSectorObjects Objects
	{
		get
		{
			return GameManager.Instance.config.mineShaftObjects;
		}
	}

	// Initial parameters for this mineshaft
	// Initial params are depth-aware
	protected MineSectorParams _initialParams;
	protected override MineSectorParams InitialParams
	{
		get
		{
			if (_initialParams == null)
			{
				_initialParams = ScriptableObject.CreateInstance<MineSectorParams>();
				_initialParams.CopyFrom(GameManager.Instance.config.mineShaftInitialParams);
				MineSectorParams.ApplyBonus(ref _initialParams, PerDepthBonus, Depth);
			}
			return _initialParams;
		}
	}

	// Bonuses received as the level increases
	protected override MineSectorParams PerLevelBonus
	{
		get
		{
			return GameManager.Instance.config.mineShaftPerLevelBonus;
		}
	}

	// Bonuses received on boost
	protected override MineSectorParams PerBoostBonus
	{
		get
		{
			return GameManager.Instance.config.mineShaftPerBoostBonus;
		}
	}

	// Bonuses received as the mineshaft is deeper
	protected MineSectorParams PerDepthBonus
	{
		get
		{
			return GameManager.Instance.config.mineShaftPerDepthBonus;
		}
	}

	public override SectorManager AssignManager(SectorManager manager)
	{
		SectorManager unassignedManager = base.AssignManager(manager);

		if (unassignedManager && !LevelManager.Instance.mineshaftManagers.Contains(unassignedManager))
		{
			LevelManager.Instance.mineshaftManagers.Add(unassignedManager);
		}
		if (manager)
			LevelManager.Instance.mineshaftManagers.Remove(manager);

		return unassignedManager;
	}

	public override SectorManager UnassignCurrentManager()
	{
		SectorManager unassignedManager = base.UnassignCurrentManager();

		if (unassignedManager && !LevelManager.Instance.mineshaftManagers.Contains(unassignedManager))
		{
			LevelManager.Instance.mineshaftManagers.Add(unassignedManager);
			unassignedManager.transform.SetParent(LevelManager.Instance.UnassignedManagersParent);
		}
		return unassignedManager;
	}


	// UI button call. Show managers list for mineshafts
	public override void ShowManagersMenu()
	{
		base.ShowManagersMenu();

		UIManager.instance.managersMenu.Show(this, LevelManager.Instance.mineshaftManagers);
	}

	public override void Start()
	{
		base.Start();

		// Set text displaying this mineshaft's number
		if (depthCount != null)
			depthCount.text = (Depth + 1).ToString();
	}
}



#if UNITY_EDITOR

[CustomEditor(typeof(MineShaft))]
public class MineShaftrEditor : MineSectorEditor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
	}
}

#endif