﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif


// Warehouse sector

public class Warehouse : MineSector
{
	// Prefab references for this sector
	protected override MineSectorObjects Objects
	{
		get
		{
			return GameManager.Instance.config.warehouseObjects;
		}
	}

	// Initial params, used on instantiation
	protected override MineSectorParams InitialParams
	{
		get
		{
			return GameManager.Instance.config.warehouseInitialParams;
		}
	}

	// Bonuses received as the level increases
	protected override MineSectorParams PerLevelBonus
	{
		get
		{
			return GameManager.Instance.config.warehousePerLevelBonus;
		}
	}

	// Bonuses received per boost
	protected override MineSectorParams PerBoostBonus
	{
		get
		{
			return GameManager.Instance.config.warehousePerBoostBonus;
		}
	}

	public override SectorManager AssignManager(SectorManager manager)
	{
		SectorManager unassignedManager = base.AssignManager(manager);
		
		if (unassignedManager)
			LevelManager.Instance.warehouseManagers.Add(unassignedManager);
		if (manager)
			LevelManager.Instance.warehouseManagers.Remove(manager);

		return unassignedManager;
	}

	public override SectorManager UnassignCurrentManager()
	{
		SectorManager unassignedManager = base.UnassignCurrentManager();

		if (unassignedManager)
		{
			LevelManager.Instance.warehouseManagers.Add(unassignedManager);
			unassignedManager.transform.SetParent(LevelManager.Instance.UnassignedManagersParent);
		}
		return unassignedManager;
	}



	// UI button callback. Show list of managers for the warehouse
	public override void ShowManagersMenu()
	{
		base.ShowManagersMenu();

		UIManager.instance.managersMenu.Show(this, LevelManager.Instance.warehouseManagers);
	}
}

#if UNITY_EDITOR

[CustomEditor(typeof(Warehouse))]
public class WarehouseEditor : MineSectorEditor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
	}
}

#endif

