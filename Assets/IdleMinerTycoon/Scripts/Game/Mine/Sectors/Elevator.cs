﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif


// Elevator sector

public class Elevator : MineSector
{
	// Elevator prefab references
	protected override MineSectorObjects Objects
	{
		get
		{
			return GameManager.Instance.config.elevatorObjects;
		}
	}

	// Initial params
	protected override MineSectorParams InitialParams
	{
		get
		{
			return GameManager.Instance.config.elevatorInitialParams;
		}
	}
	
	// Bonuses as the level increases
	protected override MineSectorParams PerLevelBonus
	{
		get
		{
			return GameManager.Instance.config.elevatorPerLevelBonus;
		}
	}

	// Bonus at each boost
	protected override MineSectorParams PerBoostBonus
	{
		get
		{
			return GameManager.Instance.config.elevatorPerBoostBonus;
		}
	}


	public override void Start()
	{
		base.Start();

		// Make sure everything is allinged. VerticalLayoutGroup does weird things
		StartCoroutine(ForceCheckpointsAlignment());
	}

	public override SectorManager AssignManager(SectorManager manager)
	{
		SectorManager unassignedManager = base.AssignManager(manager);

		if (unassignedManager)
			LevelManager.Instance.elevatorManagers.Add(unassignedManager);
		if (manager)
			LevelManager.Instance.elevatorManagers.Remove(manager);

		return unassignedManager;
	}

	public override SectorManager UnassignCurrentManager()
	{
		SectorManager unassignedManager = base.UnassignCurrentManager();

		if (unassignedManager)
		{
			LevelManager.Instance.elevatorManagers.Add(unassignedManager);
			unassignedManager.transform.SetParent(LevelManager.Instance.UnassignedManagersParent);
		}
		return unassignedManager;
	}


	// UI button callback. Show managers list
	public override void ShowManagersMenu()
	{
		base.ShowManagersMenu();

		UIManager.instance.managersMenu.Show(this, LevelManager.Instance.elevatorManagers);
	}

	// A new mineshaft has been created, generate a checkpoint and link to mineshaft's resources.
	public void LinkNewMineshaft(MineShaft mineshaft)
	{
		GameObject checkpointTemplate = collectionPoints[0].checkpoint.gameObject;
		GameObject newCheckpoint = Instantiate(collectionPoints[0].checkpoint.gameObject, collectionPoints[0].checkpoint.transform.parent);

		MinerCheckpoint newMinerCheck = new MinerCheckpoint();
		newMinerCheck.checkpoint = newCheckpoint.transform;
		newMinerCheck.resource = mineshaft.dropoffPoint.resource;
		
		collectionPoints.Add(newMinerCheck);

		StartCoroutine(ForceCheckpointsAlignment());
	}

	// This is necessary because the VerticalLayoutGroup doesn't update its children immediately
	IEnumerator ForceCheckpointsAlignment()
	{
		yield return null;

		foreach(MinerCheckpoint c in collectionPoints)
			if (c.checkpoint && c.resource)
				c.checkpoint.position = c.checkpoint.position.WithY(c.resource.transform.position.y);
	}
}

#if UNITY_EDITOR

[CustomEditor(typeof(Elevator))]
public class ElevatorEditor : MineSectorEditor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
	}
}

#endif

