﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// Class for all AI managers, controlling all mine sectors (elevator, warehouse, mineshaft)

public class SectorManager : MonoBehaviour
{
	public enum Type { Junior, Senior, Executive }
	public enum Powerup { None, WalkingSpeed, MiningSpeed, WorkerCapacity }

	// Slot where the avatar goes
	public Transform avatarSlot;

	// Button for powerups
	public Button effectButton;
	// Powerup status img
	public Image effectLoadingImg;

	[HideInInspector]
	public Type type; // Manager type
	[HideInInspector]
	public ManagerInfo.ManagerData info; // Info and params about what this manager can do
	[HideInInspector]
	public ManagerAvatars.ManagerData avatarData; // Display sprites and avatar prefabs

	// The time at which the powerup has been activated
	protected float _activationTime = float.MinValue;

	// Powerup routine
	protected Coroutine _effectRoutine;

	// Is the powerup active?
	public bool EffectIsActive
	{
		get { return (Time.time - _activationTime < info.powerupTime); }
	}

	// Is the powerup reloading?
	public bool EffectIsReloading
	{
		get { return (!EffectIsActive && (Time.time - _activationTime - info.powerupTime < info.powerupReloadTime)); }
	}

	// Sector this manager belongs to
	protected MineSector _sector;

	// Cost of this manager (useful for selling)
	[HideInInspector]
	public float managerCost = 0;


	// The manager gets disabled when not in use
	private void OnDisable()
	{
		// Remove all callbacks from miners
		if (_sector != null)
			foreach (Miner m in _sector.miners)
				m.jobDoneCallback -= OnMinersReady;

		// If effect is active, deactivate it and start the reload process.
		if (EffectIsActive)
			_activationTime = Time.time - info.powerupTime;
	}

	private void Update()
	{
		// Powerup status image
		if (effectLoadingImg)
		{
			if (EffectIsActive)
			{
				effectLoadingImg.fillAmount = 1.0f - Mathf.InverseLerp(_activationTime, _activationTime + info.powerupTime, Time.time);
			}
			else
			if (EffectIsReloading)
			{
				effectLoadingImg.fillAmount = 1.0f - Mathf.InverseLerp(_activationTime + info.powerupTime + info.powerupReloadTime, _activationTime + info.powerupTime, Time.time);
			}
			else
			{
				// Show powerup button if effect is neither active nor reloading
				if (effectButton.gameObject.activeSelf == false)
					effectButton.gameObject.SetActive(true);
			}
		}
	}

	// Buildup this manager (instantiate avatar, setup visuals, prefabs, etc)
	public virtual void Build (Type type, ManagerInfo.ManagerData info, ManagerAvatars.ManagerData avatar, float cost)
	{
		this.managerCost = cost;
		this.type = type;
		this.info = info;
		this.avatarData = avatar;

		GameObject newAvatar = Instantiate(avatar.gameAvatar, avatarSlot);
		newAvatar.transform.Defaults();
	}

	// Called by the sector when instantiated
	public virtual void Setup(MineSector sector)
	{
		_sector = sector;

		foreach (Miner m in sector.miners)
			m.jobDoneCallback += OnMinersReady;
	}

	// UI Button call. Activate powerup
	public void OnActivateEffect ()
	{
		if (EffectIsActive || EffectIsReloading)
			return;

		_activationTime = Time.time;
		effectButton.gameObject.SetActive(false);
	}

	public virtual void OnMinerSpawned (Miner miner)
	{
		UrgeMiner(miner);
		miner.jobDoneCallback += OnMinersReady;
	}

	// Miner is ready and idle callback. Pass in null to call to action all miners.
	public virtual void OnMinersReady(Miner miner)
	{
		if (miner)
		{
			// Urge miner to start its thing
			UrgeMiner(miner);
		}
		else
		{
			// No input means that we should urge all miners (on instantiation, etc)
			for (int i = 0; i < _sector.miners.Count; i++)
				UrgeMiner(_sector.miners[i]);
		}
	}

	// Call to action
	protected void UrgeMiner(Miner miner)
	{
		if (miner == null || miner.state != Miner.State.Idle)
			return;

		// Check effect and tell miner that it should apply powerups
		if (EffectIsActive)
		{
			miner.StartMining(_sector.collectionPoints, _sector.dropoffPoint, info.powerup, info.powerupAmount);
		}
		else
		{
			miner.StartMining(_sector.collectionPoints, _sector.dropoffPoint);
		}
	}
}

