﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class MinerCheckpoint // Collection or Dropoff point for a miner
{
	public ResourcePoint resource; // Where to collect or dropoff resources 
	public Transform checkpoint; // Where to go for collection/dropoff
}

// Miner base class.
public class Miner : MonoBehaviour
{
	public Resource.Type resourceType = Resource.Type.Coin; // What type of resources are we mining for?
	[ReadOnly]
	public float resourceAmount = 0; // How much are we carrying right now?

	// Current state
	public enum State { Idle, Mining }
	private State _state = State.Idle;
	public State state
	{
		get { return _state; }
		set
		{
			switch(value)
			{
				case State.Idle:
					if (idleAnimation)
						idleAnimation.enabled = true;
					break;
				case State.Mining:
					if (idleAnimation)
						idleAnimation.enabled = false;
					break;
			}
			_state = value;
		}
	}

	[Header("UI")]
	public Image avatarImage;
	public Image loadingBar;
	public Text resourceAmountText;
	public SimpleScalePulse idleAnimation;

	// This gets called when the miner has finished its collect/dropoff routine
	public System.Action<Miner> jobDoneCallback;

	// Mining routine
	protected Coroutine _miningRoutine;

	// Sector this miner belongs to
	protected MineSector _sector;

	private enum Orientation { None, Left, Right }
	private Orientation _orientation;
	private Orientation MyOrientation
	{
		get { return _orientation; }
		set
		{
			if (_orientation == value)
				return;
			switch(value)
			{
				case Orientation.Left:
					avatarImage.transform.localScale = avatarImage.transform.localScale.WithX(-Mathf.Abs(avatarImage.transform.localScale.x));
					break;
				case Orientation.Right:
					avatarImage.transform.localScale = avatarImage.transform.localScale.WithX(Mathf.Abs(avatarImage.transform.localScale.x));
					break;
			}
			_orientation = value;
		}
	}


	// Initialise miner
	public void Setup (MineSector sector)
	{
		_sector = sector;

		// UI
		if (loadingBar)
			loadingBar.fillAmount = 0;
		if (resourceAmountText)
			resourceAmountText.text = string.Empty;
		if (idleAnimation)
			idleAnimation.enabled = true;
	}

	// Urge miner to mine
	public void StartMining(List<MinerCheckpoint> collectionPoints, MinerCheckpoint dropoffPoint, SectorManager.Powerup powerup = SectorManager.Powerup.None, float powerupAmount = 0)
	{
		// If busy, do nothing
		if (state != State.Idle)
			return;
		
		UnityEngine.Assertions.Assert.IsTrue(dropoffPoint != null, "Invalid Dropoff Point!");
		UnityEngine.Assertions.Assert.IsTrue(collectionPoints != null && collectionPoints.Count > 0, "Invalid Checkpoints!");

		// Start mining routine
		_miningRoutine = StartCoroutine(MiningRoutine(collectionPoints, dropoffPoint, powerup, powerupAmount));
	}

	// Here's where all the magic happens
	protected IEnumerator MiningRoutine(List<MinerCheckpoint> checkpoints, MinerCheckpoint dropoffPoint, SectorManager.Powerup powerup = SectorManager.Powerup.None, float powerupAmount = 0)
	{
		// Begin mining
		state = State.Mining;

		// UI
		if (loadingBar)
			loadingBar.fillAmount = 0;
		if (resourceAmountText)
			resourceAmountText.text = string.Empty;

		// Check for powerups and apply them
		float speed = (powerup == SectorManager.Powerup.WalkingSpeed) ? _sector.Params.walkingSpeed * powerupAmount : _sector.Params.walkingSpeed;
		float miningSpeed = (powerup == SectorManager.Powerup.MiningSpeed) ? _sector.Params.miningSpeed * powerupAmount : _sector.Params.miningSpeed;
		float workerCapacity = (powerup == SectorManager.Powerup.WorkerCapacity) ? _sector.Params.workerCapacity * powerupAmount : _sector.Params.workerCapacity;

		// Go through all checkpoints
		for (int i=0; i<checkpoints.Count; i++)
		{
			// Get positions
			Vector3 initialLocalPos = transform.localPosition;
			Vector3 targetLocalPos = transform.parent.InverseTransformPoint(checkpoints[i].checkpoint.position);

			// Distances and time
			Vector3 vecDistance = targetLocalPos - initialLocalPos;
			float distance = vecDistance.magnitude;
			float travelTime = distance / speed;

			// Orient avatar
			if (vecDistance.x > 0)
				MyOrientation = Orientation.Right;
			else
				MyOrientation = Orientation.Left;

			// Travel to checkpoint
			float time = 0;
			while (time < travelTime)
			{
				float t = time / travelTime;
				transform.localPosition = Vector3.Lerp(initialLocalPos, targetLocalPos, t);

				time += Time.deltaTime;
				yield return null;
			}
			transform.localPosition = targetLocalPos;

			// Arrived at checkpoint, rest a bit
			yield return new WaitForSeconds(0.1f);

			// Check how many resources we can collect
			float availableAmount = checkpoints[i].resource.GetAmount(resourceType);

			// If resource value is -1, it means it's infinite
			if (availableAmount == -1)
				availableAmount = float.MaxValue;

			// Check against how much miners can carry in this sector, minus the load this miner is already carrying
			float collectAmount = Mathf.Min(availableAmount, workerCapacity - resourceAmount);

			// Check how long it will take
			float collectTime = collectAmount / miningSpeed;

			// Collect
			time = 0;
			while (time < collectTime)
			{
				float t = time / collectTime;

				// UI
				if (loadingBar)
					loadingBar.fillAmount = t;

				time += Time.deltaTime;
				yield return null;
			}

			// Check how many resources are left (maybe someone finished collecting before us)
			availableAmount = checkpoints[i].resource.GetAmount(resourceType);

			// If resource value is -1, it means it's infinite
			if (availableAmount == -1)
				availableAmount = float.MaxValue;

			// Collect resources, substract from source
			float checkAmount = Mathf.Min(collectAmount, availableAmount);
			resourceAmount += checkAmount; // Get
			checkpoints[i].resource.SubstractAmount(checkAmount, resourceType); // Substract from source

			// UI
			if (loadingBar)
				loadingBar.fillAmount = 0;
			if (resourceAmountText)
				resourceAmountText.text = resourceAmount.ToString();

			// Exit loop if capacity is full
			if (resourceAmount >= workerCapacity)
				break;
		}

		// Finished collecting, go to dropoff
		{
			// Positions
			Vector3 initialLocalPos = transform.localPosition;
			Vector3 targetLocalPos = transform.parent.InverseTransformPoint(_sector.dropoffPoint.checkpoint.position);

			// Distances and time
			Vector3 vecDistance = targetLocalPos - initialLocalPos;
			float distance = vecDistance.magnitude;
			float travelTime = distance / speed;

			// Orient avatar
			if (vecDistance.x > 0)
				MyOrientation = Orientation.Right;
			else
				MyOrientation = Orientation.Left;

			// Travel to dropoff
			float time = 0;
			while (time < travelTime)
			{
				float t = time / travelTime;
				transform.localPosition = Vector3.Lerp(initialLocalPos, targetLocalPos, t);

				time += Time.deltaTime;
				yield return null;
			}
			transform.localPosition = targetLocalPos;

			// Arrived at dropoff, unload
			float unloadTime = resourceAmount / miningSpeed;
			time = 0;
			while (time < unloadTime)
			{
				float t = time / unloadTime;

				// UI
				if (loadingBar)
					loadingBar.fillAmount = t;

				time += Time.deltaTime;
				yield return null;
			}

			// Add amount to dropoff point
			_sector.dropoffPoint.resource.AddAmount(resourceAmount, resourceType);
			resourceAmount = 0;

			// UI
			if (loadingBar)
				loadingBar.fillAmount = 0;
			if (resourceAmountText)
				resourceAmountText.text = string.Empty;
		}

		// Done!
		state = State.Idle;

		// Notify listeners that it's done
		if (jobDoneCallback != null)
			jobDoneCallback(this);
	}
}

