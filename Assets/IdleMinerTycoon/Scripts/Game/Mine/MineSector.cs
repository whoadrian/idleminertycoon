﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

// Mine sector base class. From this all major mine elements derive: Elevator, Warehouse, Mineshaft

public class MineSector : MonoBehaviour
{
	[Header("Miner Checkpoints")]
	public List<MinerCheckpoint> collectionPoints; // All collection points the miners must go through
	public MinerCheckpoint dropoffPoint; // This is where the miners drop the collected resources

	[Header("Manager")]
	public Transform managerParent; // Slot where the manager gets instantiated

	[Header("Miners")]
	public int maxMiners = 5;
	public Transform minersParent; // Slot where the miners get instantiated
	public List<Miner> miners; // All current miners

	[Header("UI")]
	public Text levelText;

	// Level of this mine sector
	private int _level = 0;
	public int Level
	{
		get { return _level; }
		set
		{
			if (levelText)
				levelText.text = value.ToString();

			RecalculateParams(); // Refresh parameters on change
			_level = value;
		}
	}

	protected SectorManager _currentManager; // Current manager
	protected PlayerManager _playerManager; // The player manager (controlled by the user, can be current or not)

	// Current manager of this sector as far as outside objects are concerned
	public SectorManager CurrentManager
	{
		get
		{
			if (_currentManager == _playerManager) // Outside objects don't see the player as a sector manager
				return null;
			return _currentManager;
		}
	}

	#region Configs

	// Current mine sector parameters
	protected MineSectorParams _params; 
	public MineSectorParams Params
	{
		get
		{
			// Make a copy of initial parameters if this is the first time calling them.
			if (_params == null)
			{
				_params = ScriptableObject.CreateInstance<MineSectorParams>();
				_params.CopyFrom(InitialParams);
			}
			// Make sure everything's set up
			RecalculateParams();
			return _params;
		}
	}

	// Prefab references. Override this for each type of sector
	protected virtual MineSectorObjects Objects {
		get { return null; }
	}

	// Initial parameters of sector. Override this for each type of sector
	protected virtual MineSectorParams InitialParams {
		get { return null; }
	}

	// Override this for each type of sector
	protected virtual MineSectorParams PerLevelBonus {
		get { return null; }
	}

	// Override this for each type of sector
	protected virtual MineSectorParams PerBoostBonus {
		get { return null; }
	}

	#endregion


	public virtual void Start()
	{
		// Remove all miners, if any
		if (miners.Count > 0)
		{
			for (int i = miners.Count - 1; i > -1; i--)
				Destroy(miners[i].gameObject);
			miners.Clear();
		}

		// Make sure the dropoff point doesn't have any coins left
		if (dropoffPoint.resource)
			dropoffPoint.resource.Init();

		// Spawn initial miners (from config)
		for (int i=0; i<Params.miners; i++)
			SpawnMiner();

		// Destroy all managers (if any)
		for (int i = managerParent.childCount-1; i>-1; i--)
			Destroy(managerParent.GetChild(i).gameObject);

		// Assign a player manager by default
		if (Objects.managers.playerManager)
		{
			// This allows the player to click on the miners.
			_playerManager = Instantiate<PlayerManager>(Objects.managers.playerManager.GetComponent<PlayerManager>(), managerParent);
			_playerManager.transform.Defaults();
			_currentManager = _playerManager;
			_playerManager.Setup(this);
		}
	} 

	// Create a miner and initialise it
	public void SpawnMiner()
	{
		// Cap miner count. TODO : this should be in params
		if (miners.Count >= maxMiners)
			return;

		UnityEngine.Assertions.Assert.IsNotNull(Objects, "Config is null!");

		GameObject newMiner = Instantiate(Objects.minerPrefab, minersParent);
		newMiner.transform.position = dropoffPoint.checkpoint.position;

		Miner newMinerScript = newMiner.GetComponent<Miner>();
		UnityEngine.Assertions.Assert.IsNotNull(newMinerScript, "Instantiated miner prefab is missing its Miner script!");

		if (newMinerScript)
		{
			miners.Add(newMinerScript);
			newMinerScript.Setup(this);
		}

		if (_currentManager)
			_currentManager.OnMinerSpawned(newMinerScript);
	}

	// Display the managers list for this type of sector
	public virtual void ShowManagersMenu()
	{
		//
	}

	// Display the menu with stats and upgrades for this mine sector
	public virtual void ShowSectorMenu()
	{
		if (UIManager.instance)
			UIManager.instance.sectorMenu.Show(this);
	}

	// Assign an instance of a manager. Returns the manager that has been unassigned (if any)
	public virtual SectorManager AssignManager (SectorManager manager)
	{
		SectorManager unasignedManager = UnassignCurrentManager();

		if (_currentManager)
			_currentManager.gameObject.SetActive(false);

		manager.transform.SetParent(managerParent);
		manager.transform.Defaults();
		manager.gameObject.SetActive(true);
		manager.Setup(this);

		_currentManager = manager;
		_currentManager.OnMinersReady(null);

		return unasignedManager;
	}

	// Unassigns the current manager and returns it (if any)
	public virtual SectorManager UnassignCurrentManager()
	{
		if (_currentManager != _playerManager)
		{
			SectorManager unassignedManager = _currentManager;
			unassignedManager.gameObject.SetActive(false);

			_currentManager = _playerManager;
			_playerManager.gameObject.SetActive(true);
			_playerManager.Setup(this);

			return unassignedManager;
		}
		return null;
	}

	// Refreshes current parameters. To be called on instantiation, level change, etc.
	protected virtual void RecalculateParams()
	{
		int oldMinerCount = (_params != null) ? _params.miners : 0;

		// Deduce params from initial values, level, boosts, etc.
		_params = MineSectorParams.CalculateCurrentParams(_level, InitialParams, PerLevelBonus, PerBoostBonus);

		// Spawn new miners
		for (int i = oldMinerCount; i < _params.miners; i++)
			SpawnMiner();
	}

	// Draw gizmos for editor niceness
	private void OnDrawGizmos()
	{
		if (collectionPoints != null)
			foreach (MinerCheckpoint m in collectionPoints)
				DrawMinerCheckpointGizmos(m);
		if (dropoffPoint != null)
			DrawMinerCheckpointGizmos(dropoffPoint);
	}

	void DrawMinerCheckpointGizmos(MinerCheckpoint m)
	{
		if (m.checkpoint && m.resource)
			Gizmos.DrawLine(m.checkpoint.position, m.resource.transform.position);
		if (m.checkpoint)
			Gizmos.DrawCube(m.checkpoint.position, new Vector3(10, 10, 10));
		if (m.resource)
			Gizmos.DrawSphere(m.resource.transform.position, 10);
	}
}

#if UNITY_EDITOR

[CustomEditor(typeof(MineSector))]
public class MineSectorEditor : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		MineSector m = (MineSector)target;

		EditorGUILayout.Space();

		// Display readonly parameters in inspector (debugging)
		if (Application.isPlaying && m.Params != null)
		{
			EditorGUILayout.FloatField("Miners", m.Params.miners);
			EditorGUILayout.FloatField("Walking Speed", m.Params.walkingSpeed);
			EditorGUILayout.FloatField("Mining Speed", m.Params.miningSpeed);
			EditorGUILayout.FloatField("Worker Capacity", m.Params.workerCapacity);
		}
	}
}

#endif
