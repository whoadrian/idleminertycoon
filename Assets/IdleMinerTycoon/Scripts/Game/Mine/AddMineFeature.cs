﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Button for adding a mineshaft

public class AddMineFeature : MonoBehaviour
{
	// Cost text display
	public Text costText;

	private float _cachedTotalCoins = -1;
	
	public void Update()
	{
		if (costText)
		{
			// Get total coins
			float totalCoins = LevelManager.Instance.warehouse.dropoffPoint.resource.GetAmount(Resource.Type.Coin);

			// Check if a change in total coins has occured
			if (_cachedTotalCoins != totalCoins)
			{
				_cachedTotalCoins = totalCoins;

				// Get total cost of a new mineshaft
				float cost = GameManager.Instance.config.additionalCosts.initialMineshaftCost;
				Utils.ApplyPercentageIncreaseOverTime(ref cost, GameManager.Instance.config.additionalCosts.mineshaftCostPercentageIncreaseWithDepth / 100.0f, LevelManager.Instance.mineShafts.Count - 1);

				// Display
				if (costText)
					costText.text = cost.ToString("0.##");

				// Green for OK, red for NOPE
				if (totalCoins < cost)
				{
					if (costText)
						costText.color = Color.red;
				}
				else
				{
					if (costText)
						costText.color = Color.green;
				}
			}
		}
	}

	// UI Button callback. Adds a mine. TO DO: Resource management should be done at a higher level.
	public void OnAddMine ()
	{
		// Figure out cost
		float totalCoins = LevelManager.Instance.warehouse.dropoffPoint.resource.GetAmount(Resource.Type.Coin);
		float cost = GameManager.Instance.config.additionalCosts.initialMineshaftCost;
		Utils.ApplyPercentageIncreaseOverTime(ref cost, GameManager.Instance.config.additionalCosts.mineshaftCostPercentageIncreaseWithDepth / 100.0f, LevelManager.Instance.mineShafts.Count - 1);

		// Check if we can afford it
		if (totalCoins < cost)
			return;

		// Create mine
		LevelManager.Instance.warehouse.dropoffPoint.resource.SubstractAmount(cost, Resource.Type.Coin);
		LevelManager.Instance.CreateNewMineshaft();
	}
}

