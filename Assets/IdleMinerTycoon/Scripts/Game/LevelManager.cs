﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Class for managing this scene / level / mine

public class LevelManager : MonoBehaviour
{
	// Singleton for easy access
	private static LevelManager _instance;
	public static LevelManager Instance
	{
		get
		{
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<LevelManager>();
			return _instance;
		}
	}
	
	[Header("Warehouse")] // This scene's warehouse
	public Warehouse warehouse;
	[HideInInspector]
	public List<SectorManager> warehouseManagers; // All warehouse managers

	[Header("Elevator")] // This scene's elevator
	public Elevator elevator;
	[HideInInspector]
	public List<SectorManager> elevatorManagers; // All elevator managers

	[Header("Mineshafts")] // This scene's mineshafts
	public List<MineShaft> mineShafts = new List<MineShaft>();
	[HideInInspector]
	public List<SectorManager> mineshaftManagers; // All mineshafts managers

	// Transform parent for unassigned managers (for neatness)
	private Transform _unassignedManagersParent;
	public Transform UnassignedManagersParent
	{
		get
		{
			if (_unassignedManagersParent == null)
				_unassignedManagersParent = new GameObject("Unassigned Managers").transform;
			return _unassignedManagersParent;
		}
	}

	public void Start()
	{
		// Debug feature, set starting resources
		if (warehouse != null && warehouse.dropoffPoint != null)
			warehouse.dropoffPoint.resource.AddAmount(GameManager.Instance.config.startingResource.amount, GameManager.Instance.config.startingResource.type);
	} 

	public void BuyManager(MineSector sector, float cost)
	{
		MineSectorObjects objectsConfig = null;
		List<SectorManager> unassignedManagersList = null;

		if (sector.GetType() == typeof(Warehouse))
		{
			objectsConfig = GameManager.Instance.config.warehouseObjects;
			unassignedManagersList = warehouseManagers;
		}
		if (sector.GetType() == typeof(MineShaft))
		{
			objectsConfig = GameManager.Instance.config.mineShaftObjects;
			unassignedManagersList = mineshaftManagers;
		}
		if (sector.GetType() == typeof(Elevator))
		{
			objectsConfig = GameManager.Instance.config.elevatorObjects;
			unassignedManagersList = elevatorManagers;
		}

		SectorManager.Type managerType = (SectorManager.Type)(Random.Range(0, System.Enum.GetValues(typeof(SectorManager.Type)).Length));
		ManagerInfo.ManagerData managerInfo = objectsConfig.managers.managersInfo.GetRandomDataForType(managerType);
		ManagerAvatars.ManagerData avatarData = objectsConfig.managers.managersAvatars.GetRandomDataForType(managerType);

		GameObject avatarObject = Instantiate(objectsConfig.managers.sectorManagerTemplate, UnassignedManagersParent);
		SectorManager manager = avatarObject.GetComponent<SectorManager>();
		manager.Build(managerType, managerInfo, avatarData, cost);

		unassignedManagersList.Add(manager);
	}

	// A manager has been sold, remove from lists
	public void ManagerSold(SectorManager manager)
	{
		Destroy(manager.gameObject);

		if (elevatorManagers.Contains(manager))
			elevatorManagers.Remove(manager);
		if (mineshaftManagers.Contains(manager))
			mineshaftManagers.Remove(manager);
		if (warehouseManagers.Contains(manager))
			warehouseManagers.Remove(manager);
	}

	// Instantiate a new mineshaft and make sure everyone knows about it
	public void CreateNewMineshaft ()
	{
		// Get first mineshaft as a template
		MineShaft template = mineShafts[0];

		// Instantiate
		MineShaft newMineshaft = Instantiate(template, template.transform.parent);
		newMineshaft.transform.SetSiblingIndex(newMineshaft.transform.GetSiblingIndex() - 1);
		mineShafts.Add(newMineshaft);
		
		// Init
		newMineshaft.Level = 0;

		// Link elevator
		elevator.LinkNewMineshaft(newMineshaft);
	}
}

