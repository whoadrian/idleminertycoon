﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
	// The object to be pooled.
	public GameObject poolObject;
	// Parent of pool objects. Leave null for root.
	public Transform poolObjectParent;

	[Header("Object Pool Options")]
	// Is this pool's size automatically increasing when requesting an object and none is available?
	public bool isFlexible = true;
	
	private Dictionary<GameObject, IPoolable> _pool = new Dictionary<GameObject, IPoolable>();

	// Default folder chosen when poolObjectParent is null
	private static Transform _defaultPoolParent;
	public static Transform DefaultPoolParent
	{
		get
		{
			if (_defaultPoolParent == null)
			{
				_defaultPoolParent = new GameObject("ObjectPools").transform;
				_defaultPoolParent.Defaults();
			}
			return _defaultPoolParent;
		}
	}

	#region public_api

	// Returns an available object component from the pool. Returns null if there is none and the pool is not flexible.
	public T Get<T>()
	{
		GameObject obj = Get();

		if (obj != null)
		{
			return obj.GetComponent<T>();
		}

		return default(T);
	}

	// Returns an available object from the pool. Returns null if there is none and the pool is not flexible.
	public GameObject Get()
	{
		foreach(KeyValuePair<GameObject, IPoolable> k in _pool)
		{
			if (k.Value.IsAvailable())
				return k.Key;
		}

		if (isFlexible)
		{
			return IncreasePool();
		}

		return null;
	}

	// Terminates and removes all pool objects.
	public void Clear()
	{
		foreach(KeyValuePair<GameObject, IPoolable> k in _pool)
		{
			if (k.Key != null && k.Value != null)
				k.Value.Terminate();
		}
		_pool.Clear();
	}

	#endregion

	#region private_api

	// Increases the size of the pool by 1 and returns the new pool object.
	GameObject IncreasePool()
	{
		if (poolObject == null)
			throw new System.NullReferenceException("Object pool's template object is null!");

		GameObject obj = Instantiate(poolObject);
		obj.transform.SetParent(poolObjectParent ? poolObjectParent : DefaultPoolParent);

		IPoolable poolObj = obj.GetComponent<IPoolable>();
		if (poolObj == null)
			throw new System.NullReferenceException("Pool Prefab " + poolObject.name + " does not implement the IPoolable interface!");
		poolObj.Initialise();

		_pool.Add(obj, poolObj);
		return obj;
	}

	void OnDestroy()
	{
		Clear();
	}

	#endregion
}
