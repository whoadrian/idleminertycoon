﻿
// To be used with and by ObjectPool.cs
public interface IPoolable
{
	void Initialise(); // Called immediately after instantiation
	bool IsAvailable(); // Is this object available for pooling?
	void Terminate(); // Called when it's about to be removed from the object pool
}
