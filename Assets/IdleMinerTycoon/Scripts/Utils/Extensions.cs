﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{

	#region Renderers

	// Multiply LineRenderer's gradients by a specific color
	public static void MultiplyGradientsByColor(this LineRenderer ln, Color color)
	{
		Gradient g = new Gradient();
		GradientColorKey[] cKeys = ln.colorGradient.colorKeys;
		GradientAlphaKey[] aKeys = ln.colorGradient.alphaKeys;
		for (int i = 0; i < cKeys.Length; i++)
			cKeys[i].color *= color;
		g.SetKeys(cKeys, aKeys);
		ln.colorGradient = g;
	}

	public static void SetColor(this LineRenderer ln, Color color)
	{
		Gradient g = new Gradient();
		GradientColorKey[] cKeys = ln.colorGradient.colorKeys;
		GradientAlphaKey[] aKeys = ln.colorGradient.alphaKeys;
		for (int i = 0; i < cKeys.Length; i++)
			cKeys[i].color = color;
		g.SetKeys(cKeys, aKeys);
		ln.colorGradient = g;
	}

	// Multiply TrailRenderer's gradients by a specific color
	public static void MultiplyGradientsByColor(this TrailRenderer ln, Color color)
	{
		Gradient g = new Gradient();
		GradientColorKey[] cKeys = ln.colorGradient.colorKeys;
		GradientAlphaKey[] aKeys = ln.colorGradient.alphaKeys;
		for (int i = 0; i < cKeys.Length; i++)
			cKeys[i].color *= color;
		g.SetKeys(cKeys, aKeys);
		ln.colorGradient = g;
	}

	public static void SetColor(this TrailRenderer ln, Color color)
	{
		Gradient g = new Gradient();
		GradientColorKey[] cKeys = ln.colorGradient.colorKeys;
		GradientAlphaKey[] aKeys = ln.colorGradient.alphaKeys;
		for (int i = 0; i < cKeys.Length; i++)
			cKeys[i].color = color;
		g.SetKeys(cKeys, aKeys);
		ln.colorGradient = g;
	}

	#endregion

	#region Transform

	public static void SetGlobalScale(this Transform transform, Vector3 globalScale)
	{
		transform.localScale = Vector3.one;
		transform.localScale = new Vector3(globalScale.x / transform.lossyScale.x, globalScale.y / transform.lossyScale.y, globalScale.z / transform.lossyScale.z);
	}

	public static void Defaults(this Transform transform, bool localPos = true, bool localRot = true, bool localScale = true)
	{
		if (localPos)
			transform.localPosition = Vector3.zero;
		if (localRot)
			transform.localRotation = Quaternion.identity;
		if (localScale)
			transform.localScale = Vector3.one;
	}

	#endregion

	#region Vectors

	public static Vector3 WithX(this Vector3 v, float value)
	{
		v.x = value;
		return v;
	}

	public static Vector3 WithY(this Vector3 v, float value)
	{
		v.y = value;
		return v;
	}

	public static Vector3 WithZ(this Vector3 v, float value)
	{
		v.z = value;
		return v;
	}

	public static Vector2 WithX(this Vector2 v, float value)
	{
		v.x = value;
		return v;
	}

	public static Vector2 WithY(this Vector2 v, float value)
	{
		v.y = value;
		return v;
	}

	public static Color WithA(this Color c, float alpha)
	{
		c.a = alpha;
		return c;
	}

	#endregion

	#region RectTransform

	public static Rect ToScreenSpace(this RectTransform transform)
	{
		Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
		Rect rect = new Rect(transform.position.x, Screen.height - transform.position.y, size.x, size.y);
		rect.x -= (transform.pivot.x * size.x);
		rect.y -= ((1.0f - transform.pivot.y) * size.y);
		return rect;
	}

	#endregion

	#region Canvas

	public static Vector2 WorldToCanvasPosition(this Canvas canvas, RectTransform canvasRect, Camera camera, Vector3 position)
	{
		Vector2 temp = camera.WorldToViewportPoint(position);

		//Calculate position considering our percentage, using our canvas size
		temp.x *= canvasRect.sizeDelta.x;
		temp.y *= canvasRect.sizeDelta.y;

		//The result is ready, but, this result is correct if canvas recttransform pivot is 0,0 - left lower corner. Fix that.
		temp.x -= canvasRect.sizeDelta.x * canvasRect.pivot.x;
		temp.y -= canvasRect.sizeDelta.y * canvasRect.pivot.y;

		return temp;
	}

	#endregion

	#region GameObject

	public static List<T> GetComponentsInChildren<T>(this GameObject parent, T component)
	{
		Transform parentTransform = parent.transform;
		List<T> result = new List<T>();
		for (int i = 0; i < parentTransform.childCount; i++)
		{
			T c = parentTransform.GetChild(i).GetComponent<T>();
			if (c != null)
			{
				result.Add(c);
			}
		}
		return result;
	}

	#endregion
}
