﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleScalePulse : MonoBehaviour
{
	public Vector3 minScale;
	public Vector3 maxScale;
	[Space]
	public float pulseSpeed;
	public bool bounce = false;

	private Transform _transform;
	private Vector3 _initialScale;

	void OnEnable ()
	{
		StartCoroutine("_PulseRoutine");
	}
	
	void OnDisable ()
	{
		StopCoroutine("_PulseRoutine");
		_transform.localScale = _initialScale;
	}

	IEnumerator _PulseRoutine ()
	{
		_transform = transform;
		_initialScale = _transform.localScale;
		_transform.localScale = minScale;

		float time = 0;
		while (true)
		{
			float t;

			if (bounce)
			{
				t = Mathf.Abs(Mathf.Sin(time * pulseSpeed * Mathf.PI));
			}
			else
			{
				t = (Mathf.Sin(time * pulseSpeed * Mathf.PI) + 1) / 2.0f;
			}

			_transform.localScale = Vector3.Lerp(minScale, maxScale, t);

			time += Time.deltaTime;
			yield return null;
		}
	}
}
