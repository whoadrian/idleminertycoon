﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericTrigger : MonoBehaviour
{
	// Trigger events
	public Action<Collider> OnTriggerEnterEvent;
	public Action<Collider> OnTriggerExitEvent;

	// All colliders that are inside
	public List<Collider> DetectedColliders { protected set; get; }


	private void OnEnable()
	{
		if (DetectedColliders == null)
			DetectedColliders = new List<Collider>();	
	}

	private void OnDisable()
	{
		// Disabling a collider won't call OnTriggerExit on 
		// colliders that are already inside, so do it here
		if (OnTriggerExitEvent != null)
			foreach (Collider c in DetectedColliders)
				OnTriggerExitEvent(c);

		DetectedColliders.Clear();
	}

	private void OnTriggerEnter(Collider other)
	{
		// Register detected collider
		DetectedColliders.Add(other);

		if (OnTriggerEnterEvent != null)
			OnTriggerEnterEvent(other);
	}

	private void OnTriggerExit(Collider other)
	{
		// Unregister detected collider
		DetectedColliders.Remove(other);

		if (OnTriggerExitEvent != null)
			OnTriggerExitEvent(other);
	}
}
