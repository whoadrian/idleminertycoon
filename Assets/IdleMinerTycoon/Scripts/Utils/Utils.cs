﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils : MonoBehaviour
{

	public static void ApplyPercentageIncreaseOverTime(ref float value, float normalizedPercentage, int times)
	{
		value *= Mathf.Pow(1 + normalizedPercentage, times);
	}
}
